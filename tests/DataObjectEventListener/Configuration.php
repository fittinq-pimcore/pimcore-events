<?php declare(strict_types=1);

namespace Test\Fittinq\Pimcore\Commands\DataObjectEventListener;

use Exception;
use Fittinq\Pimcore\Commands\DataObjectEventListener\DataObjectEventListener;
use Fittinq\Symfony\RabbitMQ\RabbitMQ\RabbitMQ;
use Pimcore\Model\DataObject\Service;
use Pimcore\Model\DataObject\TestObject;
use stdClass;
use Test\Fittinq\Symfony\RabbitMQ\RabbitMQ\AMQPChannelMock;
use Test\Fittinq\Symfony\RabbitMQ\RabbitMQ\AMQPLazyConnectionMock;

class Configuration
{
    private DataObjectEventListener $dataObjectEventListener;
    private AMQPChannelMock $channel;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        $this->channel = new AMQPChannelMock();
        $connection = new AMQPLazyConnectionMock($this->channel);
        $rabbitMQ = new RabbitMQ($connection);
        $this->dataObjectEventListener = new DataObjectEventListener($rabbitMQ);
    }

    public function configure(): DataObjectEventListener
    {
        return $this->dataObjectEventListener;
    }

    /**
     * @throws Exception
     */
    public function setUpTestObject(): TestObject
    {
        $testObject = new TestObject();
        $testObject->setKey('TestObject'.uniqid());
        $testObject->setParent(Service::createFolderByPath('Commands/CommandDispatcher/TestObject'));
        $testObject->save();

        return $testObject;
    }

    public function setUpMessage(TestObject $testObject, string $commandType): stdClass
    {
        $message = new stdClass();
        $message->objectId = $testObject->getId();
        $message->commandType = $commandType;
        return $message;
    }

    public function getChannel(): AMQPChannelMock
    {
        return $this->channel;
    }
}