<?php

declare(strict_types=1);

namespace Test\Fittinq\Pimcore\Commands\Mock;

use PHPUnit\Framework\Assert;
use Pimcore\Model\Exception\UnsupportedException;
use Symfony\Component\Lock\Key;
use Symfony\Component\Lock\LockInterface;

class LockMock implements LockInterface
{

    private bool $locked = false;
    private bool $acquired = false;

    private Key $key;

    public function __construct(Key $key) {
        $this->key = $key;
    }

    public function acquire(bool $blocking = false): bool
    {
        if ($this->locked) {
            return false;
        }

        $this->acquired = true;
        $this->locked = true;
        return true;
    }

    public function release(): void
    {
        $this->locked = false;
    }

    public function isAcquired(): bool
    {
        return $this->acquired;
    }

    public function assertKey(string $key): void
    {
        Assert::assertEquals($key, $this->key);
    }

    public function assertUnlocked(): void
    {
        Assert::assertFalse($this->locked);
    }

    public function assertIsAcquired(): void
    {
        Assert::assertTrue($this->acquired);
    }

    public function refresh(float $ttl = null)
    {
        throw new UnsupportedException('function not implemented');
    }

    public function isExpired(): bool
    {
        throw new UnsupportedException('function not implemented');
    }

    public function getRemainingLifetime(): ?float
    {
        throw new UnsupportedException('function not implemented');
    }
}