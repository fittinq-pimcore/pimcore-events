<?php declare(strict_types=1);

namespace Test\Fittinq\Pimcore\Commands\DataObjectCommandConsumer;

use Fittinq\Pimcore\Commands\DataObjectCommandConsumer\DataObjectCommandConsumer;
use Fittinq\Pimcore\Commands\Exception\CommandNotFoundException;
use Fittinq\Symfony\RabbitMQ\RabbitMQ\HeaderBag;
use PHPUnit\Framework\TestCase;
use Test\Fittinq\Pimcore\Commands\Mock\HandlerMock;
use Test\Fittinq\Pimcore\Commands\Mock\LockFactoryMock;
use Throwable;

class DataObjectCommandConsumerTest extends TestCase
{
    private DataObjectCommandConsumer $dataObjectCommandConsumer;
    private Configuration $configuration;
    private HandlerMock $handler;
    private LockFactoryMock $lockFactory;

    protected function setUp(): void
    {
        $this->configuration = new Configuration();
        $this->dataObjectCommandConsumer = $this->configuration->configure();
        $this->handler = $this->configuration->getHandlerMock();
        $this->lockFactory = $this->configuration->getLockFactory();
    }

    /**
     * @throws Throwable
     */
    public function test_classNotFoundThrowsAnCommandTypeNotFoundException()
    {
        $message = $this->configuration->createMessage(1, 'not_found');
        $this->expectException(CommandNotFoundException::class);
        $this->dataObjectCommandConsumer->handleMessage(new HeaderBag(), $message, 'pim.command', '');
    }

    /**
     * @throws Throwable
     */
    public function test_dataObjectCommandConsumerExecutesCommandHandlerWithObjectId()
    {
        $this->configuration->registerCommand('product.translate.nl_NL', $this->handler);
        $message = $this->configuration->createMessage(1, 'product.translate.nl_NL');
        $this->dataObjectCommandConsumer->handleMessage(new HeaderBag(), $message, 'pim.command', '');
        $this->handler->assertHandlerCalledWithObjectId(1);
    }

    /**
     * @throws Throwable
     */
    public function test_dataObjectHasBeenLocked()
    {
        $this->configuration->registerCommand('product.translate.nl_NL', $this->handler);

        $message = $this->configuration->createMessage(345, 'product.translate.nl_NL');

        $this->dataObjectCommandConsumer->handleMessage(new HeaderBag(), $message, 'pim.command', '');

        $this->lockFactory->assertCount(1);
        $lock = $this->lockFactory->getLock(0);

        $lock->assertKey('dataObject_345');
        $lock->assertUnlocked();
        $lock->assertIsAcquired();
    }
}