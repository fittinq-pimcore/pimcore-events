# Pimcore commands
Use Pimcore commands to setup command dispatchers, listeners and handlers. 

## Install via composer
```bash
composer require fittinq\pimcore-commands
```

## Setup examples

#### Setup Synchronous event dispatcher

```yaml
Fittinq\PimcoreCommands\Event\Dispatcher\EventDispatcher:
    class: \Fittinq\PimcoreCommands\Event\Dispatcher\SynchronousEventDispatcher
    arguments:
      $eventHandlerRepository: '@Fittinq\PimcoreCommands\Event\Handler\EventHandlerRepository'
      $logger: '@Pimcore\Log\ApplicationLogger'
```

#### Setup Asynchronous event dispatcher

```yaml
# Consuming

Fittinq\PimcoreCommands\Command\ConsumeCommandsCommand:
  arguments:
    - '@Pimcore\Log\ApplicationLogger'
    - '@DavidMaes\RabbitMQ\Consumers\Consumer'

DavidMaes\RabbitMQ\Consumers\Consumer:
  arguments:
    - '@DavidMaes\RabbitMQ\Connection'
    - 'object_data.commands'
    - 'direct'
    - 'object_data.commands'
    - '%env(PIMCORE_ENVIRONMENT)%'
    - '@Fittinq\PimcoreCommands\Event\RabbitMQHandler'

# The EventHandlerRepository is located in services_eventhandlers.yml.
Fittinq\PimcoreCommands\Event\RabbitMQHandler:
  arguments:
    - '@Fittinq\PimcoreCommands\Event\Handler\EventHandlerRepository'
    - '@Pimcore\Log\ApplicationLogger'

# Producing

Fittinq\PimcoreCommands\Event\Dispatcher\EventDispatcher:
  class: Fittinq\PimcoreCommands\Event\Dispatcher\AsynchronousEventDispatcher
  arguments:
    - '@DavidMaes\RabbitMQ\Producers\Producer'
    - '@Pimcore\Log\ApplicationLogger'

DavidMaes\RabbitMQ\Producers\Producer:
  arguments:
    - '@DavidMaes\RabbitMQ\Connection'
    - 'object_data.commands'
    - 'direct'
    - '%env(PIMCORE_ENVIRONMENT)%'

```

#### Setup Event listeners
```yaml
Fittinq\PimcoreCommands\Event\Pimcore\DataObjectEventListener:
  arguments:
    - '@Fittinq\PimcoreCommands\Event\Dispatcher\EventDispatcher'
    - '@Fittinq\PimcoreVersioning\ChangedFieldsExtractor'
    - '@Fittinq\PimcoreCommands\Event\Pimcore\MessageFactory'
  tags:
    - name: kernel.event_listener
      event: pimcore.dataobject.postUpdate
      method: onPostUpdate
  calls:
    - method: addEventListener
      arguments:
        - '@serviceA'

    - method: addEventListener
      arguments:
        - '@serviceB'
          
serviceA:
    class: \Fittinq\PimcoreCommands\Event\Listener\DataObjectEventListener
    arguments:
      - 'objectA.example.objectbrick'
      - ['field1', 'field2', 'objectbrick.field1']
      - 'Pimcore\Model\DataObject\ObjectA'
      - ['objectbrick']  #empty if there's no objectbrick
      - '@ObjectADataObjectQuerier'
        
serviceB:
  class: \Fittinq\PimcoreCommands\Event\Listener\DataObjectEventListener
  arguments:
    - 'objectB.example.no.objectbrick'
    - ['field1', 'field2']
    - 'Pimcore\Model\DataObject\Object'
    - []  #empty if there's no objectbrick
    - '@Fittinq\PimcoreDataObject\Objectbricks\ObjectbricksQuerier'

```

#### Setup Event Handlers
```yaml
Fittinq\PimcoreCommands\Event\Handler\EventHandlerRepository:
  calls:
    - method: addEventHandler
      arguments:
        - 'objectA.example.objectbrick'
        - '@HandlerA'
    - method: addEventHandler
      arguments:
        - 'objectB.example.no.objectbrick'
        - '@HandlerB'
HandlerA: ~
  HandlerADecorator:
    class: Fittinq\PimcoreCommands\Event\Handler\LockedDataObjectEventHandlerDecorator
    decorates: AppBundle\HandlerAHandler

HandlerB: ~
  HandlerADecorator:
    class: Fittinq\PimcoreCommands\Event\Handler\LockedDataObjectEventHandlerDecorator
    decorates: AppBundle\HandlerBHandler

  
```



