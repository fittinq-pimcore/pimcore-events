<?php declare(strict_types=1);

namespace Fittinq\Pimcore\Commands\DataObjectCommandConsumer;

use Fittinq\Symfony\RabbitMQ\ErrorLogging\ErrorLogger;
use Fittinq\Symfony\RabbitMQ\Handler\Handler;
use Fittinq\Symfony\RabbitMQ\RabbitMQ\HeaderBag;
use stdClass;

class DataObjectCommandConsumer extends Handler
{
    private CommandRegistry $commandRegistry;

    public function __construct(string $queue, ErrorLogger $errorLogger, CommandRegistry $commandRegistry)
    {
        $this->commandRegistry = $commandRegistry;

        parent::__construct($queue, $errorLogger);
    }

    public function handleMessage(HeaderBag $headers, array|stdClass $body, string $exchange, string $routingKey): void
    {
        $commandHandler = $this->commandRegistry->getCommand($body->commandType);
        $commandHandler->handle($body->objectId);
    }
}